﻿using UnityEngine;
using System.Collections;

public class Opening : MonoBehaviour {

    Countdown mOpeningCounter;

	void Start () {
        mOpeningCounter = new Countdown(false, 3f);
        mOpeningCounter.Start();
	}
	
	void Update () {
        mOpeningCounter.Update(Time.deltaTime, true);

        if (!mOpeningCounter.Active)
        {
            Application.LoadLevel("Main");     
        }
	}
}
