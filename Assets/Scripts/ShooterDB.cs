﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class ShooterDB : MonoBehaviour {

    List<Dictionary<string, string>> shooterDB = new List<Dictionary<string, string>>();
    Dictionary<string, string> shooterDBElem;

    static ShooterDB _instance;

    public static ShooterDB instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ShooterDB>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {

        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }

        Init();
    }

    void Init()
    {
        XmlDocument xmlDoc = new XmlDocument();
        TextAsset txml = (TextAsset)Resources.Load("DB/ShooterDB");
        xmlDoc.LoadXml(txml.text);
        XmlNodeList shooterList = xmlDoc.GetElementsByTagName("Shooter");
        foreach (XmlNode shooterInfo in shooterList)
        {
            XmlNodeList shooterContent = shooterInfo.ChildNodes;
            shooterDBElem = new Dictionary<string, string>();

            foreach (XmlNode shooterElem in shooterContent)
            {
                switch (shooterElem.Name)
                {
                    case "Index":
                        shooterDBElem.Add("Index", shooterElem.InnerText);
                        break;
                    case "Name":
                        shooterDBElem.Add("Name", shooterElem.InnerText);
                        break;
                    case "Price":
                        shooterDBElem.Add("Price", shooterElem.InnerText);
                        break;
                    default: break;

                }
            }
            shooterDB.Add(shooterDBElem);
        }
    }

    public bool GetShooterByID(int id, out string strReturn, out Dictionary<string, string> outDict)
    {
        strReturn = "";
        outDict = null;

        for (int i = 0; i < shooterDB.Count; i++)
        {
            string outString;

            if (!shooterDB[i].TryGetValue("Index", out outString))
            {
                return false;
            }

            if (id.ToString() == outString)
            {
                shooterDB[i].TryGetValue("Name", out outString);
                strReturn = outString;
                outDict = shooterDB[i];
                return true;
            }
        }

        return false;
    }
}
