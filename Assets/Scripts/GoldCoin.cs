﻿using UnityEngine;
using System.Collections;

public class GoldCoin : MonoBehaviour {

    public int gold = 1;
    public GameObject destination;
    public AudioClip coinSound;

    void OnMouseDown()
    {
        GetComponent<AudioSource>().PlayOneShot(coinSound, 1f);
        GameManager.Instance.AddGold(gold);
        iTween.MoveTo(gameObject, iTween.Hash("position", destination.transform.position,                                              
                                              "easeType", iTween.EaseType.spring,
                                              "speed", 5));
        Destroy(gameObject, 1f);
    }
}
