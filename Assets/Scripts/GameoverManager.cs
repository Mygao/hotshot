﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class GameoverManager : MonoBehaviour {

    public GameObject showAdButton;

    bool isGameOver;

	// Use this for initialization
	void Start () {
        isGameOver = false;
        Advertisement.Initialize("33064");
	}
	
	// Update is called once per frame
	void Update () {
       /* if(isGameOver)
        {
            if(Input.anyKeyDown)
            {
                Application.LoadLevel("Main");
                isGameOver = false;
            }
        }	*/
	}

    public void RestartGame()
    {
        if (isGameOver)
        {
            Application.LoadLevel("Main");
            isGameOver = false;            
        }	
    }

    public void ShowAd()
    {
        if (Advertisement.isReady()) 
        {
            Advertisement.Show(); 
            GameManager.Instance.AddGold(30 + Random.Range(0, 70));
            showAdButton.GetComponent<Button>().enabled = false;
            showAdButton.GetComponent<Image>().enabled = false;
        }
    }

    public void SetActivate()
    {
        GetComponent<Animator>().SetTrigger("TriggerGameover");
        StartCoroutine("GameoverCoroutine");
        GameManager.Instance.PostToLeaderboard();
    }

    IEnumerator GameoverCoroutine()
    {
        yield return new WaitForSeconds(1.0f);
        isGameOver = true;
    }
}
