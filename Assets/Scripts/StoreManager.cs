﻿using UnityEngine;
using System.Collections;
using Soomla.Store;

public class StoreManager : MonoBehaviour {
    public static StoreManager instance;

    public virtual void Awake()
    {
        if (!instance)
        {
            instance = this as StoreManager;
        }
        else
        {
            DestroyObject(gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }
	// Use this for initialization
	void Start () {
        SoomlaStore.Initialize(new StoreAsset());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
