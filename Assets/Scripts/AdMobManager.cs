﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdMobManager : MonoBehaviour {
    public static AdMobManager instance;
#if UNITY_ANDROID
    string adUnitId = "ca-app-pub-6338358380322447/8496952616";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif
    BannerView bannerView;
    InterstitialAd interstitial;

    public virtual void Awake()
    {
        if (!instance)
        {
            instance = this as AdMobManager;
        }
        else
        {
            DestroyObject(gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }
	// Use this for initialization
	void Start () {
       /* AdSize adSize = new AdSize(250, 25);
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, adSize, AdPosition.Top);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);

        interstitial = new InterstitialAd(adUnitId);
        // Load the interstitial with the request.
        interstitial.LoadAd(request);*/
	}
	
	public void ShowInterstitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }
}
