﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Projectile : MonoBehaviour {

    GameObject target;
    int weaponId = 0;
    string weaponName = "";
    string rarity = "";
    int damage = 1;
    float power = 1f;
    bool doneDamage = false;

    const int XThreshold = 5;
    const int YThreshold = 4;
    
    public enum DestroyType
    {
        ePiercing = 0,
        eBreaking,
        eExplosive,
        eThrough,
    }

    public DestroyType destroyType;
    GameObject prefabDestroy;
    GameObject prefabExplosion;

	void Start () {
        prefabDestroy = (GameObject)Resources.Load("Prefabs/Projectiles/DestroyEffect");
        prefabExplosion = (GameObject)Resources.Load("Prefabs/Projectiles/ExplosionEffect");
	}

    public void InitWithDBElem(Dictionary<string, string> weaponDBElem)
    {
        if (weaponDBElem == null)
        {
            return;
        }

        string outString = "";

        if (weaponDBElem.TryGetValue("Index", out outString))
        {
            weaponId = System.Convert.ToInt32(outString);
        }

        if (weaponDBElem.TryGetValue("Name", out outString))
        {
            weaponName = outString;
        }

        if (weaponDBElem.TryGetValue("Rarity", out outString))
        {
            rarity = outString;
        }

        if (weaponDBElem.TryGetValue("Attack", out outString))
        {
            damage = System.Convert.ToInt32(outString);
        }        
    }

    public void SetPower(float value)
    {
        power = value;
    }

	void FixedUpdate () {
        if (GetComponent<Rigidbody2D>().isKinematic == false)
        {
            transform.up = GetComponent<Rigidbody2D>().velocity.normalized;
        }
	}

    void Update()
    {
        if (gameObject.transform.position.x > XThreshold || gameObject.transform.position.x < -XThreshold || gameObject.transform.position.y > YThreshold)
        {
            Destroy(gameObject);
            if (!doneDamage)
            {
                GameManager.Instance.ReportMissed();
            }

        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (GetComponent<Rigidbody2D>().isKinematic == false && coll.gameObject.tag == "Enemy")
        {
            
            target = coll.gameObject;
            /*if (destroyType != DestroyType.eThrough)
            {
                //GetComponent<Rigidbody2D>().isKinematic = true;
                StartCoroutine("CoroutineSetKinematicFalse");
                this.transform.SetParent(target.transform);
            }*/
            
            if (destroyType == DestroyType.eBreaking)
            {
                Destroy(gameObject);

                GameObject spawnedObj = (GameObject)Instantiate(prefabDestroy, transform.position, Quaternion.identity);
                Destroy(spawnedObj, spawnedObj.GetComponent<ParticleSystem>().duration);
            }
            else if(destroyType == DestroyType.eExplosive)
            {
                Destroy(gameObject);

                GameObject spawnedObj = (GameObject)Instantiate(prefabExplosion, transform.position, Quaternion.identity);
                Destroy(spawnedObj, spawnedObj.GetComponent<ParticleSystem>().duration);
            }

            if (target.GetComponent<Enemy>().IsDead)
            {
                return;
            }

            if (this.transform.position.y > target.transform.position.y - 0.2)
            {
                target.GetComponent<Enemy>().HeadShot();
                GameManager.Instance.ReportHit();
                doneDamage = true;
            }
            else
            {
                target.GetComponent<Enemy>().GetDamage((int)Mathf.Round(damage * power));
                GameManager.Instance.ReportHit();
                doneDamage = true;
            }

            if (destroyType == DestroyType.eThrough)
            {
                damage = (int)(damage*(0.8f));
            }
        }

        if (GetComponent<Rigidbody2D>().isKinematic == false && coll.gameObject.tag == "Ground")
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
            target = coll.gameObject;
            this.transform.SetParent(target.transform);
            
            if (!doneDamage)
            {
                GameManager.Instance.ReportMissed();
            }

            if (destroyType == DestroyType.eExplosive)
            {
                Destroy(gameObject);

                GameObject spawnedObj = (GameObject)Instantiate(prefabExplosion, transform.position, Quaternion.identity);
                Destroy(spawnedObj, 0.5f);
            }
        }
    }


    void OnTriggerStay2D(Collider2D coll)
    {
        if (GetComponent<Rigidbody2D>().isKinematic == false && coll.gameObject.tag == "Enemy")
        {
            if (destroyType != DestroyType.eThrough)
            {
                GetComponent<Rigidbody2D>().isKinematic = true;
                this.transform.SetParent(target.transform);
            }
        }
    }

    IEnumerator CoroutineSetKinematicFalse()
    {
        yield return new WaitForSeconds(0.05f);
        GetComponent<Rigidbody2D>().isKinematic = true;
    }
}
