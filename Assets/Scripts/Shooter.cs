﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Shooter : MonoBehaviour {
    /*
     * Class to be added 1st season : Rogue, Archer, Mage, Knight, Gon, Voo, Zedi, Pitcher, SWAT, Gambler, BlindAsura,
     *                                Zeus, Lucifer, Michael, Diablo, Thor, 
     * target 100 classes + @
     */
    /* 
     * ToDo: 
     *       Unlock effect. Get Gold Effect.
     *       Drop Gold rate and quantity.
     *       Enemy classes.(Some of them may differ depending on theme.)
     *       Aiming zone and power show UI. Aim tip renderer (Arrow, cross hair..etc).
     *              Critical System. Combo Buff(like LOL).
     *       
     *               projectile animation effect. Spining projectile.
     *       
     *       Background image corresponding to classes. Theme.
     *       BGM, Sound effects.
     *       Push. Share.
     *       Rank show when the game is over
     *       Total headshot count, max combo count, multishot show
     * //Check!!
     *       stay->enter trigger
     *       Add Ad and remove slot
     *       enemy spawn by time, min 1sec
     *       
     */
    public GameObject prefabProjectile;
    public LineRenderer aimRenderer;
    public int velocityMultiplier = 8;
    public float attackSpeed = 0.3f;
    public AudioClip shootSound;
    public float rendererWidthEnd = 0.1f;
    public float rendererWidthStart = 0.3f;

    GameObject projectile;
    bool IsClicked;
    float power = 0.3f;
    const float aimGapMultiplier = 1.2f;
    Dictionary<string, string> outDict;

    Vector3 catchPoint = new Vector3(0, 0, 0);
    Vector3 releasePoint = new Vector3(0, 0, 0);

    Countdown mAttackCd;

    Image imgCoolDown;

    GameObject deadEffect;

    bool isPlaying = false;

    void Awake()
    {
        mAttackCd = new Countdown(false, attackSpeed);
    }

	void Start () 
    {
        GetComponentInChildren<Canvas>().sortingLayerName = "ForeGround";
        imgCoolDown = GetComponentInChildren<Image>();
        imgCoolDown.type = Image.Type.Filled;
        imgCoolDown.fillClockwise = false;
        imgCoolDown.fillMethod = Image.FillMethod.Radial360;
        imgCoolDown.fillOrigin = (int)Image.Origin360.Top;
        imgCoolDown.fillAmount = .0f;

        IsClicked = false;
        aimRenderer.sortingLayerName = "LineRenderer";
        GetComponent<SpriteRenderer>().sortingLayerName = "Player";
        RenderAim(transform.position, transform.position, 0); 
        LoadProjectileByWeaponName(prefabProjectile.gameObject.name);

        deadEffect = (GameObject)Resources.Load("Prefabs/Efx/DeadEffect");
	}
	
	void Update () 
    {
        if (!isPlaying) return;

        mAttackCd.Update(Time.deltaTime, true);

        if (mAttackCd.Active)
        {
            imgCoolDown.fillAmount = 1 - mAttackCd.Elapsed / mAttackCd.Initial;
        }
        else
        {
            imgCoolDown.fillAmount = 0;
        }

        if(IsClicked)
        {
            if (power <= 2)
            {
                power += 1f / 90f;
            }

           // RenderAim(Camera.main.ScreenToWorldPoint(Input.mousePosition), (-Camera.main.ScreenToWorldPoint(Input.mousePosition) + 2 * (catchPoint) - (catchPoint - transform.position)));
            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPos.z = 0;

            float magnitude = (catchPoint - mouseWorldPos).magnitude;
            if (magnitude > aimGapMultiplier)
            {
                magnitude = aimGapMultiplier;
            }
            float pow = (magnitude / aimGapMultiplier + power/4);

            Vector3 vectorRefinedStart = (mouseWorldPos - transform.position).normalized * pow + transform.position;
            Vector3 vectorRefinedEnd = (-(vectorRefinedStart - transform.position).normalized * (pow) * 1.3f + transform.position);

            RenderAim(vectorRefinedStart, vectorRefinedEnd, power);
            //RenderAim(Camera.main.ScreenToWorldPoint(Input.mousePosition), vectorRefinedEnd);
        }
        else
        {
            RenderAim(transform.position, transform.position, 0); 
        }

        if (projectile != null && projectile.GetComponent<Rigidbody2D>().isKinematic == false && !IsClicked)
        {
            ReleaseProjectile();
        }
	}

    void RenderAim(Vector3 startPos, Vector3 endPos, float pow)
    {
        startPos.z = 0;
        endPos.z = 0;
        aimRenderer.SetWidth(rendererWidthStart + pow / 4, rendererWidthEnd + pow / 10);
        aimRenderer.SetPosition(0, startPos);
        aimRenderer.SetPosition(1, endPos);
    }

    void ReleaseProjectile()
    {
        projectile.transform.up = -(releasePoint - transform.position);
        float magnitude = (catchPoint - releasePoint).magnitude;
        if (magnitude > aimGapMultiplier)
        {
            magnitude = aimGapMultiplier;
        }
        projectile.GetComponent<Rigidbody2D>().velocity = -(releasePoint - catchPoint).normalized * magnitude * velocityMultiplier * (power);
        projectile.GetComponent<Projectile>().SetPower(power);

        GetComponent<AudioSource>().PlayOneShot(shootSound, power / 2f);
        power = 1f;
        projectile = null;
    }

    void OnMouseDown()
    {
        if (!isPlaying) return;

        if (projectile == null || !IsClicked)
        {
            if (mAttackCd.Active)
            {
                return;
            }

            IsClicked = true;
            GetComponent<Animator>().SetBool("Aim", IsClicked);
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPosition.z = 0;
            catchPoint = mouseWorldPosition;
            power = 1f;
        }
    }

    void OnMouseUp()
    {
        if (!isPlaying) return;

        if(IsClicked)
        {
            projectile = (GameObject)Instantiate(prefabProjectile, transform.position, Quaternion.identity);

            projectile.GetComponent<Projectile>().InitWithDBElem(outDict);

            projectile.GetComponent<Rigidbody2D>().isKinematic = false;
            Destroy(projectile, 3f);

            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPosition.z = 0;
            releasePoint = mouseWorldPosition;

            IsClicked = false;
            GetComponent<Animator>().SetBool("Aim", IsClicked);
            GetComponent<Animator>().SetTrigger("Attack");

            mAttackCd.Start();            
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (isPlaying && coll.gameObject.tag == "Enemy")
        {
            if (!coll.gameObject.GetComponent<Enemy>().IsDead)
            {
                GameObject spawnedEfx = (GameObject)Instantiate(deadEffect, transform.position, Quaternion.identity);
                Destroy(spawnedEfx.gameObject, 0.5f);
                //this.gameObject.SetActive(false);
                isPlaying = false;
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponentInChildren<Canvas>().enabled = false;
                RenderAim(transform.position, transform.position, 0); 
                coll.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                GameObject.Find("GameoverCanvas").GetComponent<GameoverManager>().SetActivate();
                GameManager.Instance.ReportGameOver();
            }
        }
    }

    void LoadProjectileByWeaponID(int id)
    {
        string outString;

        if (WeaponDB.instance.GetWeaponByID(id, out outString, out outDict))
        {
            prefabProjectile = (GameObject)Resources.Load("Prefabs/Projectiles/" + outString);
        }
    }

    void LoadProjectileByWeaponName(string name)
    {
        if (WeaponDB.instance.GetWeaponByName(name, out outDict))
        {
            prefabProjectile = (GameObject)Resources.Load("Prefabs/Projectiles/" + name);
        }
    }

    public void SetPlay()
    {
        isPlaying = true;
    }
}
