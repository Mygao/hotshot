﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class WeaponDB : MonoBehaviour
{

    List<Dictionary<string, string>> weaponDB = new List<Dictionary<string, string>>();
    Dictionary<string, string> weaponDBElem;

    static WeaponDB _instance;

    public static WeaponDB instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<WeaponDB>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {

        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }

        Init();
    }

    void Init()
    {
        XmlDocument xmlDoc = new XmlDocument();
        TextAsset txml = (TextAsset)Resources.Load("DB/WeaponDB");
        xmlDoc.LoadXml(txml.text);
        XmlNodeList weaponList = xmlDoc.GetElementsByTagName("Weapon");
        foreach (XmlNode weaponInfo in weaponList)
        {
            XmlNodeList weaponContent = weaponInfo.ChildNodes;
            weaponDBElem = new Dictionary<string, string>();

            foreach (XmlNode weaponElem in weaponContent)
            {
                switch (weaponElem.Name)
                {
                    case "Index":
                        weaponDBElem.Add("Index", weaponElem.InnerText);
                        break;
                    case "Name":
                        weaponDBElem.Add("Name", weaponElem.InnerText);
                        break;
                    case "Rarity":
                        weaponDBElem.Add("Rarity", weaponElem.InnerText);
                        break;
                    case "Attack":
                        weaponDBElem.Add("Attack", weaponElem.InnerText);
                        break;
                    default: break;

                }
            }
            weaponDB.Add(weaponDBElem);
        }
    }

    public bool GetWeaponByID(int id, out string strReturn, out Dictionary<string, string> outDict)
    {
        strReturn = "";
        outDict = null;

        for (int i = 0; i < weaponDB.Count; i++)
        {
            string outString;

            if (!weaponDB[i].TryGetValue("Index", out outString))
            {
                return false;
            }

            if (id.ToString() == outString)
            {
                weaponDB[i].TryGetValue("Name", out outString);
                strReturn = outString;
                outDict = weaponDB[i];
                return true;
            }
        }

        return false;
    }

    public bool GetWeaponByName(string name, out Dictionary<string, string> outDict)
    {
        outDict = null;

        for (int i = 0; i < weaponDB.Count; i++)
        {
            string outString;

            if (!weaponDB[i].TryGetValue("Name", out outString))
            {
                return false;
            }

            if (name == outString)
            {
                outDict = weaponDB[i];
                return true;
            }
        }

        return false;
    }
}
