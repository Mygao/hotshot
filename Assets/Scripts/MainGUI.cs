﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainGUI : MonoBehaviour {
    private static MainGUI sInstance;
    public static MainGUI Instance
    {
        get
        {
            return sInstance;
        }
    }

    void Awake()
    {
        if (sInstance != null)
        {
            Destroy(sInstance.gameObject);
        }

        sInstance = this;
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
