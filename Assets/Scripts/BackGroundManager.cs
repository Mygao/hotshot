﻿using UnityEngine;
using System.Collections;

public class BackGroundManager : MonoBehaviour {

    private static BackGroundManager sInstance;
    public static BackGroundManager Instance
    {
        get
        {
            return sInstance;
        }
    }

    public Sprite[] backgroundPic;
    public SpriteRenderer backgroundContainer;

    void Awake()
    {
        if (sInstance != null)
        {
            Destroy(sInstance.gameObject);
        }

        sInstance = this;
    }

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetRandomBackground()
    {
        backgroundContainer.sprite = backgroundPic[Random.Range(0, backgroundPic.Length)];        
    }
}
