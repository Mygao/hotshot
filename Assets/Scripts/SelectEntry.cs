﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Soomla.Store;

public class SelectEntry : MonoBehaviour {

    bool isSelected = false;
    int index = 0;
    int price = 0;
    Vector3 originalScale = new Vector3(0.6f, 0.6f, 0.6f);
    Vector3 zoomedScale = new Vector3(1.3f, 1.3f, 1.3f);
    bool isUnlocked = false;
    public Text textName;
    VirtualGood virtualGood = null;

    void Awake()
    {
        this.transform.localScale = originalScale;
        GetComponent<Image>().color = Color.black;
        textName.color = new Color(textName.color.r, textName.color.g, textName.color.b, 0f);
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!isUnlocked)
        {
            if (virtualGood != null)
            {
                if (virtualGood.GetBalance() > 0)
                {
                    SetUnlock();
                    GameManager.Instance.SetPlayer(GetIndex());
                    GameManager.Instance.SaveGame();
                }
            }
        }

        if (this.transform.position.x < 300 && this.transform.position.x > 200)
        {
            this.transform.localScale = zoomedScale;
            isSelected = true;
            textName.color = new Color(textName.color.r, textName.color.g, textName.color.b, 1f);
            
            if (!isUnlocked && price != 0)
            {
                SelectPanel.Instance.SetButtonText(price.ToString());
            }
            else
            {
                SelectPanel.Instance.SetButtonText("");
            }

            if (!isUnlocked)
            { 

                if(virtualGood != null)
                {
                    string strPrice = ((PurchaseWithMarket)virtualGood.PurchaseType).MarketItem.MarketPriceAndCurrency;
                    if (string.IsNullOrEmpty(strPrice))
                    {
                        strPrice = ((PurchaseWithMarket)virtualGood.PurchaseType).MarketItem.Price.ToString("0.00");
                    }

                    SelectPanel.Instance.SetBuyButtonText(strPrice);  
                }                  
                
            }
            else
            {
                SelectPanel.Instance.SetBuyButtonText("");
            }
        }
        else
        {
            this.transform.localScale = originalScale;
            isSelected = false;
            textName.color = new Color(textName.color.r, textName.color.g, textName.color.b, 0f);
        }
	}

    public void SetIndex(int n)
    {
        index = n;
    }

    public int GetIndex()
    {
        return index;
    }

    public void SetUnlock()
    {
        isUnlocked = true;
        GetComponent<Image>().color = Color.white;
    }

    public bool IsUnlocked()
    {
        return isUnlocked;
    }

    public bool IsSelected()
    {
        return isSelected;
    }

    public void SetPrice(int n)
    {
        price = n;
    }

    public int GetPrice()
    {
        return price;
    }

    public void SetName(string strName)
    {
        textName.text = strName;

        foreach (VirtualGood vg in StoreInfo.Goods)
        {
            Debug.Log(vg.Name);
            if (vg.Name == textName.text)
            {
                virtualGood = vg;
            }
        }
        
    }

    public VirtualGood GetVirtualGood()
    {
        return virtualGood;
    }
}
