﻿using UnityEngine;
using System.Collections;
using Soomla.Store;
using System.Collections.Generic;

public class StoreAsset : IStoreAssets {

    public int GetVersion()
    {
        return 0;
    }

    public VirtualCurrency[] GetCurrencies()
    {
        return new VirtualCurrency[] {  };
    }
    
    public VirtualCurrencyPack[] GetCurrencyPacks()
    {
        return new VirtualCurrencyPack[] {  };
    }

    public VirtualCategory[] GetCategories()
    {
        return new VirtualCategory[] { GENERAL_CATEGORY };
    }

    public VirtualGood[] GetGoods()
    {
        return new VirtualGood[] { GHOST_LTVG, HULK_LTVG, BERSERKER_LTVG, SNIPER_LTVG, DEATHKNIGHT_LTVG, WEETCHER_LTVG, MAGE_LTVG, NO_ADS_LTVG };
    }

    public const string GHOST_LIFETIME_PRODUCT_ID = "ghost";
    public const string HULK_LIFETIME_PRODUCT_ID = "hulk";
    public const string BERSERKER_LIFETIME_PRODUCT_ID = "berserker";
    public const string SNIPER_LIFETIME_PRODUCT_ID = "sniper";
    public const string DEATHKNIGHT_LIFETIME_PRODUCT_ID = "deathknight";
    public const string WEETCHER_LIFETIME_PRODUCT_ID = "weetcher";
    public const string MAGE_LIFETIME_PRODUCT_ID = "mage";
    public const string NO_ADS_LIFETIME_PRODUCT_ID = "no_ads";

    public static VirtualGood GHOST_LTVG = new LifetimeVG(
            "Ghost", 														// name
            "My sword want to taste your soul.",				 									// description
            "ghost",														// item id
            new PurchaseWithMarket(GHOST_LIFETIME_PRODUCT_ID, 0.99));

    public static VirtualGood HULK_LTVG = new LifetimeVG(
            "Hulk", 														// name
            "Argghhh!",				 									// description
            "hulk",														// item id
            new PurchaseWithMarket(HULK_LIFETIME_PRODUCT_ID, 0.99));
    public static VirtualGood BERSERKER_LTVG = new LifetimeVG(
            "Berserker", 														// name
            "No mercy on my B.F sword.",				 									// description
            "berserker",														// item id
            new PurchaseWithMarket(BERSERKER_LIFETIME_PRODUCT_ID, 0.99));
    public static VirtualGood SNIPER_LTVG = new LifetimeVG(
            "Sniper", 														// name
            "Beware, pretty boy.",				 									// description
            "sniper",														// item id
            new PurchaseWithMarket(SNIPER_LIFETIME_PRODUCT_ID, 0.99));
    public static VirtualGood DEATHKNIGHT_LTVG = new LifetimeVG(
            "DeathKnight", 														// name
            "...",				 									// description
            "deathknight",														// item id
            new PurchaseWithMarket(DEATHKNIGHT_LIFETIME_PRODUCT_ID, 0.99));
    public static VirtualGood WEETCHER_LTVG = new LifetimeVG(
            "Weetcher", 														// name
            "Where is she?",				 									// description
            "weetcher",														// item id
            new PurchaseWithMarket(WEETCHER_LIFETIME_PRODUCT_ID, 0.99));
    public static VirtualGood MAGE_LTVG = new LifetimeVG(
            "Mage", 														// name
            "Gozauras!",				 									// description
            "mage",														// item id
            new PurchaseWithMarket(MAGE_LIFETIME_PRODUCT_ID, 0.99));

    public static VirtualGood NO_ADS_LTVG = new LifetimeVG(
            "No Ads", 														// name
            "No More Ads!",				 									// description
            "no_ads",														// item id
            new PurchaseWithMarket(NO_ADS_LIFETIME_PRODUCT_ID, 0.99));
	// Use this for initialization
    public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory(
                "General", new List<string>(new string[] { "Empty" })
        );
}
