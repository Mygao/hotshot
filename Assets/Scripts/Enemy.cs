﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {

    public GameObject target;
    public int maxHP = 10;
    int hp;
    public int dropGold = 10;
    public float speed = 2;
    bool isDead;
    public bool IsDead { get { return isDead; } }
    Transform healthbar;
    Transform healthbarBG;
    public ParticleSystem bloodEffect;
    public AudioClip bloodSound;
    GameObject destroyEffect;
    GameObject headshotEffect;
    Vector3[] pathContainer;

    public GameObject prefabGoldCoin;
    public GameObject prefabHeadshotText;

	void Start () {
        pathContainer = iTweenPath.GetPath("EnemyPath");
        pathContainer[0] = new Vector3(3, Random.Range(0f, 3f), 0);
        pathContainer[1] = new Vector3(2, Random.Range(0f, 3f), 0);
        pathContainer[2] = new Vector3(1, Random.Range(0f, 3f), 0);
        pathContainer[3] = new Vector3(0, Random.Range(0f, 3f), 0);
        pathContainer[4] = new Vector3(-1, Random.Range(0f, 3f), 0);
        pathContainer[5] = new Vector3(-2, Random.Range(0f, 3f), 0);
        iTween.MoveTo(gameObject, iTween.Hash("position", target.transform.position,
                                              "path", pathContainer,	
                                              "easeType", iTween.EaseType.linear,
                                              "speed", speed));
        hp = maxHP;
        healthbar = transform.Find("Canvas/HealthBar");
        healthbarBG = transform.Find("Canvas/HealthBarBG"); 
        isDead = false;
        destroyEffect = (GameObject)Resources.Load("Prefabs/Efx/DestroyEffect");
        headshotEffect = (GameObject)Resources.Load("Prefabs/Efx/HeadshotEffect");
        bloodEffect.Stop();
	}
    
    public void InitWithDBElem(Dictionary<string, string> enemyDBElem)
    {
        string outString = "";

        if (enemyDBElem.TryGetValue("HP", out outString))
        {
            maxHP = System.Convert.ToInt32(outString);
            hp = maxHP;
        }

        if (enemyDBElem.TryGetValue("DropGold", out outString))
        {
            dropGold = System.Convert.ToInt32(outString);
        }

        if (enemyDBElem.TryGetValue("Speed", out outString))
        {
            speed = System.Convert.ToInt32(outString);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (healthbar != null)
        {
            healthbar.localScale = new Vector3(Mathf.Abs((float)hp / (float)maxHP), healthbar.localScale.y, healthbar.localScale.z);
        }
	}

    public void GetDamage(int dmg)
    {
        bloodEffect.Stop();
        bloodEffect.Play();
        GetComponent<AudioSource>().PlayOneShot(bloodSound);
        hp -= dmg;

        if (hp <= 0)
        {
            hp = 0;
            Dead(2f);
        }
    }

    public void HeadShot()
    {
        GameObject spawnedEfx = (GameObject)Instantiate(headshotEffect, transform.position, Quaternion.identity);
        Destroy(spawnedEfx.gameObject, spawnedEfx.GetComponent<ParticleSystem>().duration);
        
        GameObject spawnedObj = (GameObject)Instantiate(prefabHeadshotText, Camera.main.WorldToScreenPoint(transform.position), Quaternion.identity);
        Destroy(spawnedObj, 2f);
        hp = 0;
        Dead(0);
    }

    void Dead(float fadeTime)
    {
        if (isDead == false)
        {
            iTween.Stop(this.gameObject);

            SpriteRenderer s = (SpriteRenderer)GetComponent<SpriteRenderer>();
            s.color = Color.red;
            
            GameObject spawnedEfx = (GameObject)Instantiate(destroyEffect, transform.position, Quaternion.identity);
            spawnedEfx.GetComponent<ParticleSystem>().startSpeed = (2.5f - fadeTime) * 2.5f;
            Destroy(spawnedEfx.gameObject, 0.5f);

            GetComponent<Rigidbody2D>().gravityScale = 1f;
            Destroy(this.gameObject, fadeTime);
            
            GameManager.Instance.AddScore(1);

            if (dropGold > 0)
            {
                GameManager.Instance.AddGold(dropGold);
            }

            if(Random.Range(0,100) > 75)
            {
                Instantiate(prefabGoldCoin, transform.position, Quaternion.identity);
            }

            healthbar.gameObject.SetActive(false);
            healthbarBG.gameObject.SetActive(false);

            GetComponent<Animator>().enabled = false;

            isDead = true;
        }
    }
}
