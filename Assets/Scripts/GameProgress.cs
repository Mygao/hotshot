﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class GameProgress
{
    private const string PlayerPrefsKey = "Hotshot-game-progress";

    List<int> mUnlockedIndex;
    public List<int> UnlockedIndex
    {
        get { return mUnlockedIndex; }
    }

    private int mGold = 0;
    public int Gold
    {
        get
        {
            return mGold;
        }
        set
        {
            mGold = value;
        }
    }

    private int mMaxScore = 0;
    public int MaxScore
    {
        get
        {
            return mMaxScore;
        }
        set
        {
            mMaxScore = value;
        }
    }

    private int mLastPlayerIndex = 0;
    public int LastPlayerIndex
    {
        get
        {
            return mLastPlayerIndex;
        }
        set
        {
            mLastPlayerIndex = value;
        }
    }

    public GameProgress()
    {
        mUnlockedIndex = new List<int>();
        mUnlockedIndex.Add(-1);
        mUnlockedIndex.Add(0);
    }

    public static GameProgress LoadFromDisk()
    {
        string s = PlayerPrefs.GetString(PlayerPrefsKey, "");
        if (s == null || s.Trim().Length == 0)
        {
            return new GameProgress();
        }
        return GameProgress.FromString(s);
    }

    public static GameProgress FromBytes(byte[] b)
    {
        return GameProgress.FromString(System.Text.ASCIIEncoding.Default.GetString(b));
    }

    public void SaveToDisk()
    {
        PlayerPrefs.SetString(PlayerPrefsKey, ToString());
    }

    public override string ToString()
    {
        string s = "GPv1:" + mGold.ToString();
        s += ":" + mMaxScore.ToString();
        s += ":" + mLastPlayerIndex.ToString();

        for (int i = 0; i < mUnlockedIndex.Count; i++ )
        {
            s += ":" + mUnlockedIndex[i];
        }

        return s;
    }

    public byte[] ToBytes()
    {
        return System.Text.ASCIIEncoding.Default.GetBytes(ToString());
    }

    public static GameProgress FromString(string s)
    {
        GameProgress gp = new GameProgress();
        string[] p = s.Split(new char[] { ':' });

        if (!p[0].StartsWith("GPv"))
        {
            Debug.LogError("Failed to parse game progress from: " + s);
            return gp;
        }
        
        gp.mGold = System.Convert.ToInt32(p[1]);
        gp.mMaxScore = System.Convert.ToInt32(p[2]);
        gp.mLastPlayerIndex = System.Convert.ToInt32(p[3]);

        for (int i = 4; i < p.Length; i++)
        {
            if (!gp.mUnlockedIndex.Contains(System.Convert.ToInt32(p[i])))
            {
                gp.mUnlockedIndex.Add(System.Convert.ToInt32(p[i]));
            }
        }

        return gp;
    }
    
    public void MergeWith(GameProgress other)
    {
        if(other.MaxScore > mMaxScore)
        {
            mMaxScore = other.MaxScore;
        }

        if(other.Gold > mGold)
        {
            mGold = other.Gold;
        }

        if(other.UnlockedIndex.Count > mUnlockedIndex.Count)
        {
            mUnlockedIndex = other.UnlockedIndex;
        }
    }

    public void AddUnlockedIndex(int n)
    {
        if (!mUnlockedIndex.Contains(n))
        {
            mUnlockedIndex.Add(n);
            Debug.Log("Added " + n);
        }
    }

    public void ResetUnlockedIndex()
    {
        mUnlockedIndex.Clear();
        mUnlockedIndex.Add(-1);
        mUnlockedIndex.Add(0);
    }

    public bool IsUnlocked(int n)
    {
        return mUnlockedIndex.Contains(n) ? true : false;
    }

    public int GetUnlockedRandomIndex()
    {
        int n = UnityEngine.Random.Range(0, mUnlockedIndex.Count);

        while (mUnlockedIndex[n] < 0)
        {
            n = UnityEngine.Random.Range(0, mUnlockedIndex.Count);
        }

        return mUnlockedIndex[n];
    }

    public int GetIndexOfUnlocked(int n)
    {
        return mUnlockedIndex.IndexOf(n);
    }
}
