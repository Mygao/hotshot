﻿using UnityEngine;
using System.Collections;

public class TextMovement : MonoBehaviour {
    public int speed = 20;
	void Update () 
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
	}
}
