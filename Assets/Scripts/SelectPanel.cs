﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Soomla.Store;

public class SelectPanel : MonoBehaviour {
    private static SelectPanel sInstance;
    public static SelectPanel Instance
    {
        get
        {
            return sInstance;
        }
    }

    List<SelectEntry> entries;

    public Sprite imageDefaultButton;
    public Sprite imageAvailableButton;
    public Button OKButton;
    public Button BuyButton;
    public AudioClip beepSound;
    public AudioClip coinSound;

    void Awake()
    {
        if (sInstance != null)
        {
            Destroy(sInstance.gameObject);
        }

        sInstance = this;
    }

	void Start () 
    {
        entries = new List<SelectEntry>();
        RefreshEntries();
        gameObject.SetActive(false);
	}

    void RefreshEntries()
    {
        GameObject contentPanel = transform.Find("ScrollView/ContentPanel").gameObject;

        string outName;
        Dictionary<string, string> outDict;
        int i = -1;

        while (ShooterDB.instance.GetShooterByID(i, out outName, out outDict))
        {
            GameObject prefabEntry = (GameObject)Resources.Load("Prefabs/" + "ShooterEntry");
            
            GameObject newEntry = Instantiate(prefabEntry) as GameObject;
            
            newEntry.transform.SetParent(contentPanel.transform);
            newEntry.GetComponent<SelectEntry>().SetName(outName);

            GameObject thumbNail = (GameObject)Resources.Load("Prefabs/" + outName + "_Thumb");
            newEntry.GetComponent<Image>().sprite = thumbNail.GetComponent<SpriteRenderer>().sprite;
            newEntry.GetComponent<SelectEntry>().SetIndex(i);
            
            string outPrice;
            
            if (outDict.TryGetValue("Price", out outPrice))
            {
                newEntry.GetComponent<SelectEntry>().SetPrice(System.Convert.ToInt32(outPrice));
            }
            
            entries.Add(newEntry.GetComponent<SelectEntry>());
            
            i++;
        }

        GameObject prefabEntryComingSoon = (GameObject)Resources.Load("Prefabs/" + "ShooterEntry");

        GameObject newEntryComingSoon = Instantiate(prefabEntryComingSoon) as GameObject;

        newEntryComingSoon.transform.SetParent(contentPanel.transform);
        newEntryComingSoon.GetComponent<SelectEntry>().SetName("Coming Soon!");
        GameObject thumbNailComingSoon = (GameObject)Resources.Load("Prefabs/" + "ComingSoon" + "_Thumb");
        newEntryComingSoon.GetComponent<Image>().sprite = thumbNailComingSoon.GetComponent<SpriteRenderer>().sprite;
        newEntryComingSoon.GetComponent<SelectEntry>().SetIndex(i);
        newEntryComingSoon.GetComponent<SelectEntry>().SetPrice(0);
        newEntryComingSoon.GetComponent<SelectEntry>().GetComponent<Image>().color = Color.white;
        entries.Add(newEntryComingSoon.GetComponent<SelectEntry>());

    }
	
	void Update () 
    {
	   
	}

    public void TogglePanel()
    {
        this.gameObject.SetActive(!this.gameObject.activeSelf);
        GetComponent<AudioSource>().PlayOneShot(beepSound, 1f);

        if (this.gameObject.activeSelf)
        {
            SoomlaStore.StartIabServiceInBg();
        }
        else
        {
            SoomlaStore.StopIabServiceInBg();
        }
    }

    /* ToDo: Separate buying sequence to the Buy Button. */
    public void OnButtonOK()
    {
        GameManager gm = GameManager.Instance;
        GetComponent<AudioSource>().PlayOneShot(beepSound, 1f);

        for(int i = 0; i < entries.Count - 1 ; i++)
        {
            if(entries[i].IsSelected())
            {
                if (entries[i].IsUnlocked())
                {
                    gm.SetPlayer(entries[i].GetIndex());
                    this.gameObject.SetActive(false);
                    gm.StartGame(); 
                    break;
                }

                if (gm.UseGold(entries[i].GetPrice()))
                {
                    //ToDo:: Unlock Effect
                    GetComponent<AudioSource>().PlayOneShot(coinSound, 1f);
                    gm.SetPlayer(entries[i].GetIndex());
                    entries[i].SetUnlock();
                }
                else
                {
                    return;
                }
                
            }
        }       
    }

    public void OnButtonBuy()
    {
        GameManager gm = GameManager.Instance;

        for (int i = 0; i < entries.Count - 1; i++)
        {
            if (entries[i].IsSelected())
            {
                if (entries[i].IsUnlocked())
                {
                    return;
                }

                try
                {
                    VirtualGood vg = entries[i].GetVirtualGood();           
                    StoreInventory.BuyItem(vg.ItemId);     
                }
                catch (System.Exception e)
                {
                    Debug.Log("SOOMLA/UNITY " + e.Message);
                }
            }
        }       
    }

    public void SetElemUnlock(int index)
    {
        for(int i = 0; i < entries.Count - 1; i++)
        {
            if(entries[i].GetIndex() == index)
            {
                entries[i].SetUnlock();
                break;
            }
        }
    }

    public void SetButtonText(string txt)
    {
        if (txt == "" || txt == "0")
        {
            GetComponentInChildren<Button>().GetComponent<Image>().overrideSprite = null;
            GetComponentInChildren<Button>().GetComponentInChildren<Text>().text = "";
        }
        else
        {
            if (GameManager.Instance.CheckAffordability(System.Convert.ToInt32(txt)))
            {
                GetComponentInChildren<Button>().GetComponent<Image>().overrideSprite = imageAvailableButton;
            }
            else
            {
                GetComponentInChildren<Button>().GetComponent<Image>().overrideSprite = imageDefaultButton;
            }
            GetComponentInChildren<Button>().GetComponentInChildren<Text>().text = txt + "G";
        }        
    }

    public void SetBuyButtonText(string txt)
    {
        if (txt == "" || txt == "0")
        {
            BuyButton.GetComponentInChildren<Text>().text = txt;
            BuyButton.enabled = false;
            BuyButton.GetComponent<Image>().enabled = false;
        }
        else
        {
            BuyButton.GetComponentInChildren<Text>().text = txt;
            BuyButton.enabled = true;
            BuyButton.GetComponent<Image>().enabled = true;
        }
    }

    public void ResetPurchases()
    {
        for (int i = 0; i < entries.Count - 1; i++)
        {
            VirtualGood vg = entries[i].GetVirtualGood();
            if (vg != null)
            {
                vg.Take(1);
                vg.ResetBalance(0);
            }
            break;            
        }
    }
}
