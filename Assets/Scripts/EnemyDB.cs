﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class EnemyDB : MonoBehaviour {
    List<Dictionary<string, string>> enemyDB = new List<Dictionary<string, string>>();
    Dictionary<string, string> enemyDBElem;

    static EnemyDB _instance;

    public static EnemyDB instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<EnemyDB>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {

        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }

        Init();
    }

    void Init()
    {
        XmlDocument xmlDoc = new XmlDocument();
        TextAsset txml = (TextAsset)Resources.Load("DB/EnemyDB");
        xmlDoc.LoadXml(txml.text);
        XmlNodeList enemyList = xmlDoc.GetElementsByTagName("Enemy");
        foreach (XmlNode enemyInfo in enemyList)
        {
            XmlNodeList enemyContent = enemyInfo.ChildNodes;
            enemyDBElem = new Dictionary<string, string>();

            foreach (XmlNode enemyElem in enemyContent)
            {
                switch (enemyElem.Name)
                {
                    case "Index":
                        enemyDBElem.Add("Index", enemyElem.InnerText);
                        break;
                    case "Name":
                        enemyDBElem.Add("Name", enemyElem.InnerText);
                        break;
                    case "HP":
                        enemyDBElem.Add("HP", enemyElem.InnerText);
                        break;
                    case "DropGold":
                        enemyDBElem.Add("DropGold", enemyElem.InnerText);
                        break;
                    case "Speed":
                        enemyDBElem.Add("Speed", enemyElem.InnerText);
                        break;

                    default: break;

                }
            }
            enemyDB.Add(enemyDBElem);
        }
    }

    public bool GetEnemyByID(int id, out string strReturn, out Dictionary<string, string> outDict)
    {
        strReturn = "";
        outDict = null;

        for (int i = 0; i < enemyDB.Count; i++)
        {
            string outString;

            if (!enemyDB[i].TryGetValue("Index", out outString))
            {
                return false;
            }

            if (id.ToString() == outString)
            {
                enemyDB[i].TryGetValue("Name", out outString);
                strReturn = outString;
                outDict = enemyDB[i];
                return true;
            }
        }

        return false;
    }
}
