﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine.SocialPlatforms;


public class GameManager : MonoBehaviour {
    private static GameManager sInstance;
    public static GameManager Instance
    {
        get
        {
            return sInstance;
        }
    }
    public AudioClip beepSound;

    public int totalShooterNumber = 7;
    public GameObject playerSpawnPoint;
    GameObject spawnedPlayer;
    public GameObject prefabPlayer;
    int playerIndex = 0;

    public GameObject enemySpawnPoint;
    public GameObject prefabEnemy;
    GameObject spawnedEnemy;
    Countdown spawnTime;

    public GameObject indexFinger;
    Countdown userNoInput;

    public Text maxScoreText;
    public Text scoreText;
    public Text goldText;
    public GameObject goldImage;
    public Text comboText;

    int gameScore = 0;
    int gameLevel = 0;
    int gameGold = 0;
    int gameCombo = 0;
    int gameMaxScore = 0;
    int mHighestPostedScore = 0;

    Countdown mComboCd;

    bool isPaused = false;

    private GameProgress mProgress;

    private bool mAuthenticating = false;
    public bool Authenticating
    {
        get
        {
            return mAuthenticating;
        }
    }

    public bool Authenticated
    {
        get
        {
            return Social.Active.localUser.authenticated;
        }
    }

    private bool mSaving;

    void Awake()
    {
        if (sInstance != null)
        {
            Destroy(sInstance.gameObject);
        }

        sInstance = this;
        Screen.SetResolution(480, 320, true);
        prefabPlayer = (GameObject)Resources.Load("Prefabs/Ghost");
        isPaused = true;
        
        spawnTime = new Countdown(false, 3f);
        mComboCd = new Countdown(false, 2.5f);
        userNoInput = new Countdown(false, 2f);
        
        comboText.gameObject.SetActive(false);

        Authenticate();
    }

	void Start () 
    {
        mProgress = GameProgress.LoadFromDisk();
        LoadFromCloud();

        gameGold = mProgress.Gold;
        gameMaxScore = mProgress.MaxScore;
        playerIndex = mProgress.LastPlayerIndex;
        SetPlayer(playerIndex);

        for (int i = -1; i < totalShooterNumber; i++)
        {
            if(mProgress.IsUnlocked(i))
            {
                SelectPanel.Instance.SetElemUnlock(i);
            }
        }

        maxScoreText.text = "Top " + gameMaxScore;
        scoreText.text = "" + gameScore;
        goldText.text = "" + gameGold;
        //Time.timeScale = 0;

        indexFinger.transform.position = playerSpawnPoint.transform.position;
        iTween.MoveBy(indexFinger, iTween.Hash("x", -1.2,
                                              "easeType", iTween.EaseType.easeInOutSine,
                                              "loopType","pingPong",
                                              "speed", 2));
        indexFinger.gameObject.SetActive(false);

        BackGroundManager.Instance.SetRandomBackground();
        SpawnPlayer();
	}

    public void Authenticate()
    {
        if (Authenticated || mAuthenticating)
        {
            Debug.LogWarning("Ignoring repeated call to Authenticate().");
            return;
        }

        // Enable/disable logs on the PlayGamesPlatform
        PlayGamesPlatform.DebugLogEnabled = true;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                .EnableSavedGames()
                .Build();
        PlayGamesPlatform.InitializeInstance(config);

        // Activate the Play Games platform. This will make it the default
        // implementation of Social.Active
        PlayGamesPlatform.Activate();

        // Set the default leaderboard for the leaderboards UI
        ((PlayGamesPlatform)Social.Active).SetDefaultLeaderboardForUI("CgkIuILznOYFEAIQAQ");

        // Sign in to Google Play Games
        mAuthenticating = true;
        Social.localUser.Authenticate((bool success) =>
        {
            mAuthenticating = false;
            if (success)
            {
                // if we signed in successfully, load data from cloud
                Debug.Log("Login successful!");
                LoadFromCloud();
                gameGold = mProgress.Gold;
                gameMaxScore = mProgress.MaxScore;
                playerIndex = mProgress.LastPlayerIndex;
                SetPlayer(playerIndex);

                for (int i = -1; i < totalShooterNumber; i++)
                {
                    if (mProgress.IsUnlocked(i))
                    {
                        SelectPanel.Instance.SetElemUnlock(i);
                    }
                }

                mProgress.SaveToDisk();

                maxScoreText.text = "Top " + gameMaxScore;
                scoreText.text = "" + gameScore;
                goldText.text = "" + gameGold;
            }
            else
            {
                // no need to show error message (error messages are shown automatically
                // by plugin)
                Debug.LogWarning("Failed to sign in with Google Play Games.");
            }
        });
    }
    
    void ProcessCloudData(byte[] cloudData)
    {
        if (cloudData == null) {
            Debug.Log("No data saved to the cloud yet...");
            return;
        }
        Debug.Log("Decoding cloud data from bytes.");
        GameProgress progress = GameProgress.FromBytes(cloudData);
        Debug.Log("Merging with existing game progress.");
        mProgress.MergeWith(progress);
    }
	
    public void LoadFromCloud()
    {
        Debug.Log("Loading game progress from the cloud.");
        mSaving = false;

        if (((PlayGamesPlatform)Social.Active).SavedGame == null)
        {
      
            Debug.Log("PlayGamesPlatform Null");
        }
        else
        { 
            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution("AutoSave",
                                                                                             DataSource.ReadCacheOrNetwork,
                                                                                             ConflictResolutionStrategy.UseOriginal,
                                                                                             SavedGameOpened);
        }
    }

    public void SavedGameSelected(SelectUIStatus status, ISavedGameMetadata game)
    {

        if (status == SelectUIStatus.SavedGameSelected)
        {
            string filename = game.Filename;
            Debug.Log("opening saved game:  " + game);
            if (mSaving && (filename == null || filename.Length == 0))
            {
                filename = "save";
            }
            //open the data.
            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution(filename,
                                                                                             DataSource.ReadCacheOrNetwork,
                                                                                             ConflictResolutionStrategy.UseLongestPlaytime,
                                                                                             SavedGameOpened);
        }
        else
        {
            Debug.LogWarning("Error selecting save game: " + status);
        }

    }

    public void SaveToCloud()
    {
        if (Authenticated)
        {
            Debug.Log("Saving progress to the cloud...");
            mSaving = true;
            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution("AutoSave",
                                                                                       DataSource.ReadCacheOrNetwork,
                                                                                       ConflictResolutionStrategy.UseOriginal,
                                                                                       SavedGameOpened);
        }
    }

    public void SavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            if (mSaving)
            {
                Debug.Log("Saving to " + game);
                byte[] data = mProgress.ToBytes();
                SavedGameMetadataUpdate.Builder builder = new
                    SavedGameMetadataUpdate.Builder();

                SavedGameMetadataUpdate updatedMetadata = builder.Build();
                ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(game, updatedMetadata, data, SavedGameWritten);
            }
            else
            {
                Debug.Log("Load from " + game);
                ((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData(game, SavedGameLoaded);
            }
        }
        else
        {
            Debug.LogWarning("Error opening game: " + status);
        }
    }

    public void SavedGameLoaded(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            Debug.Log("SaveGameLoaded, success=" + status);
            ProcessCloudData(data);
        }
        else
        {
            Debug.LogWarning("Error reading game: " + status);
        }
    }

    public void SavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            Debug.Log("Game " + game.Description + " written");
        }
        else
        {
            Debug.LogWarning("Error saving game: " + status);
        }
    }

	void Update () 
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                //quit application on back button
                if (SelectPanel.Instance.gameObject.activeSelf)
                {
                    SelectPanel.Instance.TogglePanel();
                }
                else
                {
                    //Toggle Quit Panel
                    // Application.Quit();
                }
                return;
            }
        }

        if (isPaused)
        {
            return;
        }

        mComboCd.Update(Time.deltaTime);
        userNoInput.Update(Time.deltaTime);
        spawnTime.Update(Time.deltaTime);

        if (Input.anyKeyDown || Input.anyKey)
        {
            indexFinger.gameObject.SetActive(false);
            userNoInput.Start();
        }

        if (userNoInput.Expired)
        {
            indexFinger.gameObject.SetActive(true);
        }

        if (!mComboCd.Expired)
        {
            comboText.color = new Color(comboText.color.r, comboText.color.g, comboText.color.b, (2.5f - mComboCd.Elapsed) / 2.5f);
        }
        else
        {
            comboText.gameObject.SetActive(false);
            gameCombo = 0;
        }

        if (spawnTime.Expired)
        {
            SpawnEnemy();

            if (spawnTime.Initial > 1.0f)
            {
                spawnTime.Start(spawnTime.Initial - 0.1f);
                Debug.Log(spawnTime.Initial);
            }
            else
            {
                spawnTime.Start();
            }
        }
	}

    void OnDestroy()
    {
        mProgress.SaveToDisk();
        SaveToCloud();
    }

    void SpawnPlayer()
    {
        if(spawnedPlayer != null)
        {
            Destroy(spawnedPlayer.gameObject);
        }        

        spawnedPlayer = (GameObject)Instantiate(prefabPlayer, playerSpawnPoint.transform.position, playerSpawnPoint.transform.rotation);
    }

    void SpawnEnemy()
    {
        if(gameLevel >= 7)
        {
            gameLevel = 7;
        }

        string outString;
        Dictionary<string, string> outDict;

        if (EnemyDB.instance.GetEnemyByID(gameLevel++, out outString, out outDict))
        {
            GameObject prefabEnemy = (GameObject)Resources.Load("Prefabs/" + outString);
            Vector3 spawnPoint = enemySpawnPoint.transform.position;
            spawnPoint.y = Random.Range(0, 3f);

            spawnedEnemy = (GameObject)Instantiate(prefabEnemy, spawnPoint, enemySpawnPoint.transform.rotation);

            spawnedEnemy.GetComponent<Enemy>().InitWithDBElem(outDict);
        }
    }

    public void AddScore(int score)
    {
        gameScore += score;
        scoreText.text = "" + gameScore;
        if(gameScore > gameMaxScore)
        {
            gameMaxScore = gameScore;
            mProgress.MaxScore = gameMaxScore;
            maxScoreText.text = "Top " + gameMaxScore;
            PostToLeaderboard();
        }
    }

    public void AddGold(int gold)
    {
        gameGold += gold;
        goldText.text = "" + gameGold;
        goldText.GetComponent<Animator>().SetTrigger("GoldAdd");
        goldImage.GetComponent<Animator>().SetTrigger("GoldAdd");
        mProgress.Gold = gameGold;
        SaveToCloud();
    }

    public bool UseGold(int gold)
    {
        if(gameGold - gold < 0)
        {
            return false;
        }

        gameGold -= gold;
        goldText.text = "" + gameGold;

        SaveGame();

        return true;
    }

    public bool CheckAffordability(int gold)
    {
        if(gameGold - gold < 0)
        {
            return false;
        }

        return true;
    }

    public void SaveGame()
    {
        mProgress.SaveToDisk();
        SaveToCloud();
    }


    public void SetPlayer(int n)
    {
        if (n == -1)
        {
            playerIndex = mProgress.GetUnlockedRandomIndex();
        }
        else
        {
            playerIndex = n;
            mProgress.AddUnlockedIndex(playerIndex);
        }

        mProgress.LastPlayerIndex = n;

        string outString;
        Dictionary<string, string> outDict;

        if (ShooterDB.instance.GetShooterByID(playerIndex, out outString, out outDict))
        {
            prefabPlayer = (GameObject)Resources.Load("Prefabs/" + outString);
        }
    }

    public void StartGame()
    {
        GetComponent<AudioSource>().PlayOneShot(beepSound, 1f);
        MainGUI.Instance.Disable();
        SpawnPlayer();
        SpawnEnemy();
        spawnedPlayer.GetComponent<Shooter>().SetPlay();
        isPaused = false;
        spawnTime.Start();
        indexFinger.gameObject.SetActive(true);
        mProgress.AddUnlockedIndex(playerIndex);
        SaveGame();
    }

    public void ReportGameOver()
    {
        isPaused = true;
        spawnTime.Stop();
        userNoInput.Stop();
        indexFinger.gameObject.SetActive(false);
        //AdMobManager.instance.ShowInterstitial();
    }

    public void ResetProgress()
    {
        gameGold = 0;
        mProgress.Gold = 0;
        mProgress.ResetUnlockedIndex();
        SelectPanel.Instance.ResetPurchases();
        SaveGame();
    }

    public void ReportHit()
    {
        gameCombo++;
        comboText.text = gameCombo + " Combo!";
        mComboCd.Start();
        comboText.gameObject.SetActive(true);
        comboText.GetComponent<Animator>().SetTrigger("ComboAdd");
    }

    public void ReportMissed()
    {
        gameCombo = 0;
        //mComboCd.Stop();
        mComboCd.Start();
        comboText.gameObject.SetActive(true);
        comboText.text = "Missed!";
        comboText.GetComponent<Animator>().SetTrigger("ComboAdd");
    }

    public void PostToLeaderboard()
    {
        int score = mProgress.MaxScore;
        if (Authenticated && score > mHighestPostedScore)
        {
            // post score to the leaderboard
            Social.ReportScore(score, "CgkIuILznOYFEAIQAQ", (bool success) => { });
            mHighestPostedScore = score;
        }
        else
        {
            Debug.LogWarning("Not reporting score, auth = " + Authenticated + " " +
                               score + " <= " + mHighestPostedScore);
        }
    }

    public void ShowLeaderboardUI()
    {
        if (Authenticated)
        {
            GetComponent<AudioSource>().PlayOneShot(beepSound, 1f);
            Social.ShowLeaderboardUI();
        }
    }
}
